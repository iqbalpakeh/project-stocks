package stocks.product;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class ProductController {

    @CrossOrigin(origins = "http://localhost:3000")
    @RequestMapping("/products")
    public List<Product> products() {
        List<Product> products = new ArrayList<>();
        products.add(new Product("Sporting Goods", "$49.99", true, "Football"));
        products.add(new Product("Sporting Goods", "$9.99", true, "Baseball"));
        products.add(new Product("Sporting Goods", "$29.99", true, "Basketball"));
        products.add(new Product("Electronics", "$99.99", true, "iPod Touch"));
        products.add(new Product("Electronics", "$399.99", true, "iPhone 5"));
        products.add(new Product("Electronics", "$299.99", true, "Samsung S7"));
        return products;
    }


}
