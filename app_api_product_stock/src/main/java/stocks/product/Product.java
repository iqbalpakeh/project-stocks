package stocks.product;

public class Product {

    private final String category;
    private final String price;
    private final boolean stocked;
    private final String name;

    public Product(String category, String price, boolean stocked, String name) {
        this.category = category;
        this.price = price;
        this.stocked = stocked;
        this.name = name;
    }

    public String getCategory() {
        return category;
    }

    public String getPrice() {
        return price;
    }

    public boolean isStocked() {
        return stocked;
    }

    public String getName() {
        return name;
    }
}
