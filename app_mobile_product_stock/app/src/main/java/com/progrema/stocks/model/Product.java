package com.progrema.stocks.model;

import com.google.gson.annotations.SerializedName;

public class Product {

    @SerializedName("category")
    private final String category;

    @SerializedName("price")
    private final String price;

    @SerializedName("stocked")
    private final boolean stocked;

    @SerializedName("name")
    private final String name;

    public Product(String category, String price, boolean stocked, String name) {
        this.category = category;
        this.price = price;
        this.stocked = stocked;
        this.name = name;
    }

    public String getCategory() {
        return category;
    }

    public String getPrice() {
        return price;
    }

    public boolean isStocked() {
        return stocked;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "category = " + category + ", price = " + price + ", isStocked = " + stocked + ", name = " + name;
    }

}

