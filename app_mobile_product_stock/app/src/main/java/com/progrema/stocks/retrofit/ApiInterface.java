package com.progrema.stocks.retrofit;

import com.progrema.stocks.model.Product;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface ApiInterface {

    @GET("/products")
    Call<List<Product>> products();
}
