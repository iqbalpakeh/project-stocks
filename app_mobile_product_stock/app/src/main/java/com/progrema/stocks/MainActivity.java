package com.progrema.stocks;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.progrema.stocks.model.Product;
import com.progrema.stocks.retrofit.ApiClient;
import com.progrema.stocks.retrofit.ApiInterface;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "DBG";
    private TextView mResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mResult = findViewById(R.id.result_text);

        ApiInterface client = ApiClient.getClient().create(ApiInterface.class);
        Call<List<Product>> call = client.products();
        call.enqueue(new Callback<List<Product>>() {
            @Override
            public void onResponse(Call<List<Product>> call, Response<List<Product>> response) {
                Log.d(TAG, "RETROFIT RESPONSE OK");
                Log.d(TAG, "HTTP RAW: " + response.raw());
                Log.d(TAG, "HTTP CODE: " + response.code());
                Log.d(TAG, "HTTP HEADERS: " + response.headers());
                if (response.isSuccessful()) {
                    Log.d(TAG, "HTTP RAW: " + response.body());
                    List<Product> products = response.body();
                    for (Product product : products) {
                        mResult.append("\n" + product);
                        Log.d(TAG, "\n");
                        Log.d(TAG, "category = " + product.getCategory());
                        Log.d(TAG, "price = " + product.getPrice());
                        Log.d(TAG, "stocked = " + product.isStocked());
                        Log.d(TAG, "name = " + product.getName());
                    }
                } else {
                    Log.d(TAG, "HTTP ERROR BODY: " + response.errorBody());
                }
            }

            @Override
            public void onFailure(Call<List<Product>> call, Throwable t) {
                Log.d(TAG, "RETROFIT RESPONSE ERROR");
                t.printStackTrace();
            }
        });

    }

}
